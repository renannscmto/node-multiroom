const renderLogin = async (req, res) => {
    req.assert('username', 'Name is not most be empty').notEmpty();
    req.assert('username', 'Name most contains of 3 to 15 characters').len(3, 15);
    let error = req.validationErrors()

    if(error) {
        res.render("index", { check: error })
        return
    }
    res.render("index", {})
}

const renderChat = async (req, res) => {
    const {username} = req.body
    res.render("chat")
}

module.exports = {
    renderLogin,
    renderChat
}