const express = require("express")
const router = express.Router()

const controllerRouter = require("./controllers/controllerRouter")

router.get("/", controllerRouter.renderLogin)
router.post("/chat", controllerRouter.renderChat)

module.exports = router