const app = require("./app")
require("dotenv").config()

let server = app.listen(process.env.PORT_SERVER, () => console.log(`Server running in port: ${process.env.PORT_SERVER}`))
const io = require("socket.io")(server) // Atribuindo o servidor para ser executado juntamente com o "socket".

// Agora você pode usar o objeto 'io' para lidar com eventos de WebSocket
io.on("connection", (socket) => {
    console.log("Client connected");
    // Aqui você pode adicionar lógica para lidar com eventos de WebSocket
});