const express = require("express")
const consig = require("consign")
const bodyParser = require("body-parser")
const expressValidator = require("express-validator")
const router = require("./router")

const app = express();
app.use(express.json())
app.use(expressValidator())

// configurando as variáveis do 'express'.
app.set('view engine', 'ejs')      // Configurando que o tipo de leitura de views será em EJS.
app.set('views', './assets/views') // Configurando o caminho de onde estão as views.

// Congfigurando o middleware 'express.static()'
app.use(express.static('./assets/'))
app.use(bodyParser.urlencoded({extended: true}))

app.use(router)

// Configurando todos os diretórios que deverão ser lidos.
consig().include('controllers')
        .into(app)

module.exports = app